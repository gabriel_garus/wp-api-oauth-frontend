
(function($){
    $('#get').on( 'click', function(event) {
        event.preventDefault();

        $('.ajax-loader').show();
        // Get REST URL and post ID from WordPress
        console.log('click');
      //
        var json_url = $('#json_url').html();
        var user_query = $('#user_query').val();
        var query_url = json_url + user_query;

        
           
        var oauthToken          = $('#oauth').attr('oauthToken');
        var oauthTokenSecret    = $('#oauth').attr('oauthTokenSecret');
        var oauthConsumerKey    = $('#oauth').attr('oauthConsumerKey');
        var oauthConsumerSecret = $('#oauth').attr('oauthConsumerSecret');
        var tokens = [];

        if(typeof(oauthToken) !== 'undefined')
        {
             tokens.push( 'oauth_token=' + oauthToken );
        }

        if(typeof(oauthTokenSecret) !== 'undefined')
        {
             tokens.push('oauth_token_secret=' + oauthToken);
        }

         if(typeof(oauthConsumerKey) !== 'undefined')
        {
             tokens.push('oauth_consumer_key=' + oauthConsumerKey);
        }

         if(typeof(oauthConsumerSecret) !== 'undefined')
        {
             tokens.push('oauth_consumer_secret=' + oauthConsumerSecret);
        }




        //console.log(tokens);

        var tokens_url = tokens.join('&');
        query_url += '?' + tokens_url;

         console.log(query_url);

        // The AJAX
        $.ajax({
            dataType: 'json',
            url: query_url
        })

        .done(function(response){
            console.log(response);
             response_html = '';
            $.each(response, function(index, object) {

                  
               response_html += JSON.stringify(object) ;
               response_html += '<br /><hr><br>';
               

               
                
            });
        
            $('.ajax-loader').remove();
        
            $('#loaded-content').html(response_html);
        })

        .fail(function(error){
             $('.ajax-loader').remove();
            console.log('Disaster!!!!!');
            console.log(error);
               $('#loaded-content').html(JSON.stringify(error));
        })

        .always(function(){
            console.log('Complete');
        });

    });
})(jQuery);


function iterate(object)
{
    var response_html =  '<ul>'; 

    for(var key in object)
    {   
        var value = object[key];
    
        response_html += '<li>';
        if(typeof value === 'object')
        {
            response_html += iterate(value);
        }
        else
        {
            response_html += key + ':' + object[key];
        }
          
    }
    response_html += '</ul>';

    return response_html;
}