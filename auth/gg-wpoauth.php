<?php
/*
*  WpAuth Clsss
*  by: Gabriel Garus
*
*/


class ggWpAuth
{
	
	public $consumer_key = '';
	public $consumer_secret = '';

	// Main Oauth Token
	public $oauth_token = '';
	public $oauth_token_secret = '';

	// TEMP token
	public $request_token = '';
	public $request_token_secret = '';

	public $url = '';

	public $request_token_url = '';
	public $authorize_url    = '';
	public $access_token_url = '';

	public $oauth_callback   = '';

	public $query_url = '';

	public $oauth_version = '1.0';
	public $oauth_signature_method = 'HMAC-SHA1';

	public $response = '';

	public $oauth_verifier = '';

   public function __construct($args = array())
   {
   		if(!empty($args['consumerKey']))
   		{
   			$this->consumer_key = $args['consumerKey'];
   		}

   		if(!empty($args['consumerSecret']))
   		{
   			$this->consumer_secret = $args['consumerSecret'];
   		}

   		if(!empty($args['url']))
   		{
   			$this->url = $args['url'];
   		}

   		if(!empty($args['callbackUrl']))
   		{
   			$this->oauth_callback = $args['callbackUrl'];
   		}

   		if(!empty($_SESSION['oauthToken']))
   		{
   			$this->oauth_token = $_SESSION['oauthToken'];
   		}

   		if(!empty($_SESSION['oauthTokenSecret']))
   		{
   			$this->oauth_token_secret = $_SESSION['oauthTokenSecret'];
   		}

   		$this->generate_access_urls();
   }

   public function generate_access_urls()
   {
   		if(!empty($this->url))
   		{
   			$this->request_token_url = $this->url."/oauth1/request"; 
   			$this->authorize_url     = $this->url."/oauth1/authorize";
   			$this->access_token_url  = $this->url."/oauth1/access"; 
   		}
   		
   }

   public function setConsumerKey($val)
   {
   		$this->consumer_key = $val;
   }

   public function setConsumerSecret($val)
   {
   		$this->consumer_secret = $val;
   }

   public function setUrl($val)
   {
   		$this->url = $val;
 		$this->generate_access_urls();
   }

   public function setCallbackUrl($val)
   {
   		$this->oauth_callback = $val;
   }

   public function setQueryUrl($val)
   {
   	 $this->query_url = $val; 
   }




   public function curl_call($url)
   {
   	 $curl = curl_init();
	        curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url
		));
	 $response = curl_exec($curl);

	 return $response; 
   }

  public function curl_post_call($url)
   {
   	 $curl = curl_init();
	 
	 curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url,
		    CURLOPT_POST => 1
		));
	 $response = curl_exec($curl);

	 return $response; 
   }


/************************************************************
	*
	*   Get Temporary cridentials and save them in the response variable 
	*
	*************************************************************/
   public function get_temp_cridentials()
   {
   	
		//error_log('Getting TEMP cridentials');

		$method = 'POST';

		$requestTokenUrl = $this->request_token_url;
		
		//error_log('$requestTokenUrl: ' . $requestTokenUrl);

		// generate timestamp and nonce
	    $oauthTimestamp = time();
	    $nonce = md5(mt_rand()); 


	    // set query parameters
	    $keys = array(
	    	'oauth_consumer_key'     => $this->consumer_key,
	    	'oauth_nonce'            => $nonce,
	    	'oauth_signature_method' => $this->oauth_signature_method,
	        'oauth_timestamp'        => $oauthTimestamp,
	        'oauth_version'          => $this->oauth_version
	    	);

	    // join parameters into string
	    $joined_keys = '';
	    foreach ($keys as $key => $value)
	    {
	    	$joined_keys .= $key . '=' . $value . '&';
	    }

	    // URL encode parameters string
	    $joined_keys = rawurlencode( rtrim($joined_keys, '&') );
	      
	    // get full url with parameters string as BASE for creating signature 
	    $sigBase = "GET&" . rawurlencode($requestTokenUrl) . "&" . $joined_keys;

	    // Create signature KEY
	    $sigKey = $this->consumer_secret . "&"; 

	   // error_log('Local Signature Base: ' . rawurldecode($sigBase) );	
	   //	error_log('Local Signature Key: ' . $sigKey);
	   		//error_log('Local hash Algo: ' . $hash_algo);
	    // create SIGNATURE
	    $oauthSig = base64_encode(hash_hmac("sha1", $sigBase, $sigKey, true));


	    // create url encoded string of parameters
	    $url_keys = '';
	    foreach ($keys as $key => $value)
	    {
	    	$url_keys .= $key . '=' . rawurlencode($value) . '&';
	    }

	    // add signature
	    $url_keys .= 'oauth_signature=' . rawurlencode($oauthSig); 
	    
	    // Create request URL
	    $requestUrl = $requestTokenUrl . "?" .   $url_keys; 
	   
	   // error_log('TEMP Request Url - '. $requestUrl );
	    
	    // get response from the server
	    $response = $this->curl_call($requestUrl);
	   	$_SESSION['response'] = $response;        
     
        parse_str(trim($response), $values);

	    if(!empty($values["oauth_token"]))
	    {
	    	$_SESSION["requestToken"] = $values["oauth_token"];
	    	$this->request_token = $values["oauth_token"];
	    }
	    else
	    {
	    	error_log('temp token not received');
	    	return false;
	    }




	    if(!empty($values["oauth_token_secret"]))
	    {	
	    	$_SESSION["requestTokenSecret"] = $values["oauth_token_secret"];
	    	$this->request_token_secret     = $values["oauth_token_secret"];
	    	
	    }
	    else
	    {
	    	error_log('temp token secret not received');
	    	return false; 
	    }
		
		return $response; 


	}

	/************************************************************
	*
	*   Authorize - redirect to Wordpress to login 
	*
	*************************************************************/
	public function authorize( $resp = null)
	{
	
		$request_token = $this->get_temp_token();

		if($request_token !== null)
		{
			$redirectUrl = $this->authorize_url;
			//$redirectUrl .= '@gabriel:Qaiadamyxsl.77';
			$redirectUrl .= "?oauth_token=" . $_SESSION["requestToken"] . '&oauth_callback='. $this->oauth_callback; 
			$redirectUrl .= '&user_login=gabriel';
	   		//var_dump($redirectUrl);
        	header("Location: " . $redirectUrl);
		}
		else
		{
			error_log('Cannot Authorize: Request Token is Missing');

		}
		
	}
	//---------- Function Ends Here -----------------------

	public function login()
	{
		$this->get_temp_cridentials();

		$this->wp_login();

		$this->exchange_tokens();
	}



	public function wp_login( $username = null, $password = null )
	{	
		if($username === null)
		{
			$_SESSION['response'] = 'WP Username Required';
			return false;
		}

		if( $password === null)
		{
			$_SESSION['response'] = 'WP Password Required';
			return false;
		}


		$request_token = $this->get_temp_token();

		if($request_token !== null)
		{
			$redirectUrl = $this->authorize_url;
			//$redirectUrl .= '@gabriel:Qaiadamyxsl.77';
			$redirectUrl .= "?oauth_token=" . $_SESSION["requestToken"] . '&oauth_callback='. $this->oauth_callback; 
			$redirectUrl .= '&user_name=' . $username;
			$redirectUrl .= '&user_password=' . $password;
	   		//var_dump($redirectUrl);
        	$response = $this->curl_call($redirectUrl);
        	
        	$_SESSION['response'] = $response;

        	parse_str(trim($response), $values);

        	$_SESSION['response_values'] = $values;
        	if(!empty($values["oauth_verifier"]))
	    	{
	    		$this->oauth_verifier = $values["oauth_verifier"];
	    		return $values["oauth_verifier"];
	    	}
	    	else
	    	{
	    		return false;
	    	}
	    
        	
		}
		else
		{
			error_log('Cannot Authorize: Request Token is Missing');
			return 'Cannot Authorize: Request Token is Missing';
		}
		
	}
	//---------- Function Ends Here -----------------------



	public function get_temp_token()
	{
		 
		if(!empty($this->request_token))
		{
			$request_token = $this->request_token; 
		}
		elseif(!empty($_SESSION['requestToken']))
		{
			$request_token = $_SESSION['requestToken'];
		}
		else
		{
			$request_token = null;
		}

		return $request_token;
	}

	public function get_temp_token_secret()
	{
		
		if(!empty($this->request_token_secret))
		{
			$request_token_s = $this->request_token_secret; 
		}
		elseif(!empty($_SESSION['requestTokenSecret']))
		{
			$request_token_s = $_SESSION["requestTokenSecret"];
		}
		else
		{
			$request_token_s = null;
		}

		return $request_token_s;
	}











	/************************************************************
	*
	*    Exchange temporary token for main access token
	*
	*************************************************************/
	public function exchange_tokens($verifier = null)
	{
		if($verifier == null)  
		{
			$oauthVerifier = $this->getVerifier();	
		}
		else
		{
			$oauthVerifier = $verifier;
		}

		
		$accessTokenUrl = $this->access_token_url; 

		$request_token = $this->get_temp_token();
		$request_token_secret = $this->get_temp_token_secret();


		
		if($request_token == null)
		{
			error_log('Cannot Exchange Tokens - temporary token is missing');
		}

		if($request_token_secret == null)
		{
			error_log('Cannot Exchange Tokens - temporary token secret is missing');
		}

		if($oauthVerifier == null)
		{
			error_log('Cannot Exchange Tokens - Verifier is missing');
		}



		// Create nonce and timestamp
	    $nonce = md5(mt_rand()); 
	    $oauthTimestamp = time();
	    
	    

	    $keys = array(
	    	'oauth_consumer_key'     => $this->consumer_key,
	    	'oauth_nonce'            => $nonce,
	    	'oauth_signature_method' => $this->oauth_signature_method,
	        'oauth_timestamp'        => $oauthTimestamp,
	        'oauth_token'			 => $request_token,
	        'oauth_verifier'         => $oauthVerifier,
	        'oauth_version'          => $this->oauth_version
	       
	    	);

	    $joined_keys = '';
	    foreach ($keys as $key => $value)
	    {
	    	$joined_keys .= $key . '=' . $value . '&';
	    }

	    $joined_keys = rawurlencode( rtrim($joined_keys, '&') );

	    $sigBase = "GET&" . rawurlencode($accessTokenUrl) . "&";
	    $sigBase .= $joined_keys; 

	    //error_log('sigbase: ' . $sigBase);

	    $sigKey = $this->consumer_secret . "&" . $request_token_secret; 

	    //error_log('Client key: ' . $sigKey );
	    $oauthSig = base64_encode(hash_hmac("sha1", $sigBase, $sigKey, true));


	    $url_keys = '';

	    foreach ($keys as $key => $value)
	    {
	    	$url_keys .= $key . '=' . rawurlencode($value) . '&';
	    }

	    $url_keys .= 'oauth_signature=' . rawurlencode($oauthSig); 
	    
	 
	    $requestUrl = $accessTokenUrl . "?" .   $url_keys;  
	 
	    $response = $this->curl_call($requestUrl);

	    if(!empty($response))
	    {	
	    	 parse_str(trim($response), $values);
	    	 
	    	 // Clear Session
	    	 //$_SESSION =  array();

	    	if(!empty($values["oauth_token"]))
	    	{
	    		$this->oauth_token = $values["oauth_token"];
	    		$_SESSION["oauthToken"] = $values["oauth_token"];
	    	}
	    	
	    	if(!empty($values["oauth_token_secret"]))
	    	{
				$this->oauth_token_secret = $values["oauth_token_secret"];
				$_SESSION["oauthTokenSecret"] = $values["oauth_token_secret"];
			}

			//$_SESSION['oauthConsumerKey']    = $this->consumer_key;
			//$_SESSION['oauthConsumerSecret'] = $this->consumer_secret;
	    }
	  

	    //error_log('request Url: ' .  $requestUrl );
     	return $values; 
	}
	//---------- Function Ends Here -----------------------
















	/************************************************************
	*
	*    Get WP Verifier   - from GET or from SESSION
	*
	*************************************************************/


	public function getVerifier()
	{	
		if(!empty($this->oauth_verifier))
		{
			$oauthVerifier = $this->oauth_verifier;
		}
		elseif(!empty($_GET['oauth_verifier']))
		{
			$oauthVerifier = $_GET['oauth_verifier'];
			//error_log('Verifier from GET');
		}
		elseif( !empty($_SESSION['oauth_verifier']))
		{
			$oauthVerifier = $_SESSION['oauth_verifier'];
			//error_log('Verifier from SESSION');
		}
		else
		{	
			$oauthVerifier = null;
		}

		return $oauthVerifier;
	}
	//---------- Function Ends Here -----------------------










	/************************************************************
	*
	*    Query WP API  - works with or without authentication
	*
	*************************************************************/

	public function query($query)
	{		
		
	
		$params = array();
		$base_query = $query;
		  if(strrpos($query, '?') == false)
		    {
		    	$joiner = '?';
		    	$base_joiner = rawurlencode('&');
		    }
		    else
		    {
		    	//mylog('query is there');
		    	$joiner = '&';
		    	$base_joiner = rawurlencode('?');
		    	$ex = explode('?', $query);

		    	$params_string = $ex[1];
		    	$base_query = $ex[0];

		    	//mylog('ex: ', $ex);

		    	$px = explode('&', $params_string);

		    	foreach ($px as $param)
		    	{
		    		$eqx = explode('=', $param);
		    	//	error_log('eqx: ' . print_r($eqx) );
		    		if(sizeof($eqx > 1))
		    		{	
		    			$key = $eqx[0];
		    			$params[$key] = $eqx[1];
		    		}
		    	}

		    }

		
		$get_url = $this->query_url . $base_query;
		if(!empty($params['lang']))
		{
		//	$get_url .= 'lang=' .$params['lang'];
		}

		
		// error_log('oauth_token: ' . $this->oauth_token);

		if(!empty( $this->oauth_token )) // User Authorized - logged in
		{
	    	$nonce = md5(mt_rand()); 
		    $oauthTimestamp = time();
		    
		    

		    $keys = array(
		    	'oauth_consumer_key'     => $this->consumer_key,
		    	'oauth_nonce'            => $nonce,
		    	'oauth_signature_method' => $this->oauth_signature_method,
		        'oauth_timestamp'        => $oauthTimestamp,
		        'oauth_token'			 => $this->oauth_token,
		        'oauth_version'          => $this->oauth_version
		       
		    	);

		    $keys = array_merge( $params, $keys);
		     uksort( $keys, 'strcmp' );

		    //mylog('the keys: ', $keys);
		    $joined_keys = '';
		    foreach ($keys as $key => $value)
		    {
		    	$joined_keys .= $key . '=' . $value . '&';
		    }

		    $joined_keys = rawurlencode( rtrim($joined_keys, '&') );

			$query_string = $this->create_signature_string( $keys  );

			$sigBase = 'GET&' .  rawurlencode( str_replace('?', '&', $get_url) ). '&';
		
			$sigBase .= $query_string; 

		    //error_log('Front : get url ' . rawurldecode($get_url));	
			
	        $sigKey = $this->consumer_secret . '&' . $this->oauth_token_secret; 
	       	
	    	
			//error_log('$this->oauth_token_secret: ' . $this->oauth_token_secret);	
	        $hash_algo = 'sha1';

	      	error_log('Local Signature Base: ' . rawurldecode($sigBase) );	
	   		//error_log('Local Signature Key: ' . $sigKey);
	   		//error_log('Local hash Algo: ' . $hash_algo);


	        $oauthSig = base64_encode( hash_hmac($hash_algo, $sigBase, $sigKey, true) );
	          				   		
			//error_log('Local Signature: ' . $oauthSig);	

		    $url_keys = '';

		    foreach ($keys as $key => $value)
		    {
		    	$url_keys .= $key . '=' . rawurlencode($value) . '&';
		    }

		    $url_keys .= 'oauth_signature=' . rawurlencode($oauthSig); 
		    
		  
		    $requestUrl =  $get_url . '?' .  $url_keys;  
		}
		else // Not Logged In
		{
			$requestUrl =  $get_url . '?' . $params_string;
		}

		//error_log('request url: ' . $requestUrl);
       
	    $response = $this->curl_call($requestUrl);

	
		//error_log('response: ' . $response);

	    // parse_str(trim($response), $values);
	    if(!empty($response))
	    {	
			return $response;
		}
		else
		{
			return false;
		}
	}
	//---------- Function Ends Here -----------------------



	public function add_post($title, $content = '', $status = 'publish', $post_type = 'posts')
	{
		//mylog('query is there');
		$joiner = '&';
		$base_joiner = rawurlencode('?');
		
		$post_url = $this->query_url . $post_type;


		// error_log('oauth_token: ' . $this->oauth_token);

		if(!empty( $this->oauth_token )) // User Authorized - logged in
		{
	    	$nonce = md5(mt_rand()); 
		    $oauthTimestamp = time();
		    
		    

		    $keys = array(
		    	'oauth_consumer_key'     => $this->consumer_key,
		    	'oauth_nonce'            => $nonce,
		    	'oauth_signature_method' => $this->oauth_signature_method,
		        'oauth_timestamp'        => $oauthTimestamp,
		        'oauth_token'			 => $this->oauth_token,
		        'oauth_version'          => $this->oauth_version,
		       	'title'					 => rawurlencode($title),
		       	'content'				 => rawurlencode($content),
		       	'status'				 => $status
		    	);

		    
		     uksort( $keys, 'strcmp' );

		   mylog('the keys: ', $keys);
		    $joined_keys = '';
		    foreach ($keys as $key => $value)
		    {
		    	$joined_keys .= $key . '=' . $value . '&';
		    }

		    $joined_keys = rawurlencode( rtrim($joined_keys, '&') );

			$query_string = $this->create_signature_string( $keys  );

			$sigBase = 'POST&' .  rawurlencode( str_replace('?', '&', $post_url) ). '&';
		
			$sigBase .= $query_string; 

		    //error_log('Front : get url ' . rawurldecode($get_url));	
			
	        $sigKey = $this->consumer_secret . '&' . $this->oauth_token_secret; 
	       	
	    	
			//error_log('$this->oauth_token_secret: ' . $this->oauth_token_secret);	
	        $hash_algo = 'sha1';

	         error_log('Local Signature Base: ' . rawurldecode($sigBase) );	
	   		 error_log('Local Signature Key: ' . $sigKey);
	   		//error_log('Local hash Algo: ' . $hash_algo);


	        $oauthSig = base64_encode( hash_hmac($hash_algo, $sigBase, $sigKey, true) );
	          				   		
			//error_log('Local Signature: ' . $oauthSig);	

		    $url_keys = '';

		    foreach ($keys as $key => $value)
		    {
		    	$url_keys .= $key . '=' . $value . '&';
		    }

		    $url_keys .= 'oauth_signature=' . rawurlencode($oauthSig); 
		    
		  
		    $requestUrl =  $post_url  . '?' .  $url_keys;  
		}
		else // Not Logged In
		{
			 $_SESSION['response'] = 'User not logged in';
			 return false;
		}

		//error_log('request url: ' . $requestUrl);
       
	    $response = $this->curl_post_call($requestUrl);

	
		//error_log('response: ' . $response);

	    // parse_str(trim($response), $values);
	    if(!empty($response))
	    {	
			return $response;
		}
		else
		{
			return false;
		}
	}




	public function join_with_equals_sign( $params, $query_params = array(), $key = '' ) 
	{
		foreach ( $params as $param_key => $param_value ) 
		{
			if ( is_array( $param_value ) ) 
			{
				$query_params = $this->join_with_equals_sign( $param_value, $query_params, $param_key );
			} else {
				if ( $key )
				{

					$param_key = $key . '[' . $param_key . ']'; // Handle multi-dimensional array
				}
				$string = $param_key . '=' . $param_value; // join with equals sign
				$query_params[] = urlencode( $string );
			}
		}
		return $query_params;
	}

	public function create_signature_string( $params ) 
	{
		return implode( '%26', $this->join_with_equals_sign( $params ) ); // join with ampersand
	}






} // END OF CLASS



function mylog($text, $val = null)
{
	$message = $text;

	if($val !== null)
	{
		if(is_array($val))
		{
			$message .= print_r($val, true);
		}
		else
		{
			$message .= $val;
		}
	}

	error_log($message);
}